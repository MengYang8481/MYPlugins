/*
* 没啥好看的，看你的配置去吧~
* 路径: ./Plugins/Vote/Config.json
* 都自定义成这样了还看源码qwq...
*/

var conf = new JsonConfigFile('.\\Plugins\\Vote\\Config.json',JSON.stringify({
    'Number': 3,
    'Text':"投票清理",
    'Cmd':"cl",
    'massage':{
        'AlreadyVoted':"§5[投票] §c你已经参与了投票 !",
        'SuccessfulVoting':"§5[投票] §c投票成功.票数 +1 !",
        'TheNumberIsFull':"§5[投票] §c设定人数已满,执行清理指令 !"
    }
}));

function Main() {
    var vote = 0 ;

    mc.regPlayerCmd('vote',conf.get('Text'),(pl) => {
        if(pl.hasTag('Vote')) {
            pl.tell(conf.get('massage')['AlreadyVoted']);
            return false;
        }
        else {
            pl.addTag('Vote');
            pl.tell(conf.get('massage')['SuccessfulVoting']);
            vote ++ ;
            if(vote == conf.get('Number'))
            {
                mc.broadcast(conf.get('massage')['TheNumberIsFull']);
                vote = 0;
                mc.runcmdEx('tag @a remove Vote');
                mc.runcmdEx(conf.get('Cmd'));
            }
        }     
    })
    mc.listen('onJoin', (pl) => {
        mc.runcmdEx('tag ' + pl.realName + ' remove Vote');
        vote --;
    })
}

Main();

log('=============================');
log('如首次使用请在配置文件修改需求');
log('如有建议以及bug反馈请前往评论');
log('作者:洋洋༊༢ (MengYang8481)');
log('=============================');

