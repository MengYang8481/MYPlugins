var conf = new JsonConfigFile('.\\Plugins\\VoteToKick\\Config.json');
conf.init('VoteNumber', 3);
var ticket = '0';
var TPlayer = 'MengYang8481';
var VoBe = '0';
function Load() {
	mc.regPlayerCmd('vk', '投票踢出恶意玩家', Form);
	mc.regPlayerCmd('vote', '投票', Vote);
	mc.listen('onJoin', (pl) = >{
		if (pl.hasTag('Vote') && VoBe == 0) {
			mc.runcmdEx('tag ' + pl.realName + ' remove Vote');
		}
	})
}
function Form(pl) {
	let fm = mc.newSimpleForm();
	fm.setTitle('投票踢人');
	fm.setContent('请选择即将投票踢出的玩家:');
	let pn = mc.getOnlinePlayers();
	pn.forEach((p) = >{
		fm.addButton(p.name);
	});
	pl.sendForm(fm,
	function(pl, id) {
		if (VoBe == 1) {
			pl.tell('§d[投票踢出] §c当前有未结束的投票');
			return false;
		}
		if (id != null) {
			let KickPlayer = pn[id].name;
			log(pl.realName + ' 对 ' + KickPlayer + ' 发起了投票踢出!');
			kp = KickPlayer;
			TPlayer = KickPlayer;
			VoBe = 1;
			mc.broadcast('§d[投票踢出] §c有玩家对 ' + kp + ' 提交投票踢出\n§d[投票踢出] §c使用 /vote 来支持此次踢出');
			return TPlayer,
			VoBe;
		} else {
			pl.tell('表单已放弃!');
		}
	});
}
function Vote(pl) {
	if (VoBe == 0) {
		pl.tell('§d[投票踢出] §c当前暂无投票踢出');
		return false;
	}
	if (!pl.hasTag('Vote')) {
		pl.addTag('Vote');
		ticket++;
		pl.tell('§d[投票踢出] §a投票成功');
		mc.broadcast('§d[投票踢出] §a当前 §c' + ticket + ' §a票');
		if (ticket == conf.get('VoteNumber')) {
			mc.broadcast('§d[投票踢出] §g对玩家 §c' + TPlayer + ' §g的投票踢出已通过');
			ticket = 0;
			VoBe = 0;
			mc.runcmdEx('tag @a remove Vote');
			mc.runcmdEx('kick "' + TPlayer + '"');
			return ticket,
			VoBe;
		}
	} else {
		pl.tell('§d[投票踢出] §c请勿重复投票');
		return false;
	}
}
Load();
log('如有建议以及bug反馈请前往评论');
log('作者:洋洋༊༢ (MengYang8481)');